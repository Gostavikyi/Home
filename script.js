window.addEvent('domready', function(){
	var el = $$('a'),
		color = el.getStyle('backgroundColor');
	
	$$('a').addEvents({
    mouseenter: function(){
	  this.set('morph', { duration: 100 });
      this.morph({
		'color': '#B4B4B4'
      });
    },

    mouseleave: function(){
		this.set('morph', { duration: 250 });
		this.morph({
        'color': '#444'
      });
    }
	});

	$('menu').addEvents({
		'mouseenter': function(){
			this.set('tween', {
				duration: 300,
				transition: Fx.Transitions.Sine.easeInOut
			}).tween('height', '300px');
		},
		'mouseleave': function(){
			this.set('tween', {}).tween('height', '100px');
		}
	});

	$('menut').addEvents({
		'mouseenter': function(){
			this.set('tween', {
				duration: 300,
				transition: Fx.Transitions.Sine.easeInOut
			}).tween('height', '300px');
		},
		'mouseleave': function(){
			this.set('tween', {}).tween('height', '100px');
		}
	});

	$('menuth').addEvents({
		'mouseenter': function(){
			this.set('tween', {
				duration: 300,
				transition: Fx.Transitions.Sine.easeInOut
			}).tween('height', '300px');
		},
		'mouseleave': function(){
			this.set('tween', {}).tween('height', '100px');
		}
	});

	$('menuf').addEvents({
		'mouseenter': function(){
			this.set('tween', {
				duration: 300,
				transition: Fx.Transitions.Sine.easeInOut
			}).tween('height', '300px');
		},
		'mouseleave': function(){
			this.set('tween', {}).tween('height', '100px');
		}
	});
	
	$('menufi').addEvents({
		'mouseenter': function(){
			this.set('tween', {
				duration: 300,
				transition: Fx.Transitions.Sine.easeInOut
			}).tween('height', '300px');
		},
		'mouseleave': function(){
			this.set('tween', {}).tween('height', '100px');
		}
	});
});
